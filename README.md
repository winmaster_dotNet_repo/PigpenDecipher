# Simple tool made to decipher Pigpen cipher

## The Pigpen cipher is a substitution mono-alphabetic algorithm used by Freemasons

## Key:

![](screens/key.jpg)

# Idea:
![](screens/idea.jpg)

# Application:
![](screens/program.jpg)
