﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PigpenDecipher
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            textBoxSolution.Text = "";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "A";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "B";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "C";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "D";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "E";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "F";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "G";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "H";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "I";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "N";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "O";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "P";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "Q";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "R";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "S";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "T";
        }

        private void button17_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "U";
        }

        private void button18_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "V";
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "J";
        }

        private void button20_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "K";
        }

        private void button21_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "L";
        }

        private void button22_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "M";
        }

        private void button23_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "W";
        }

        private void button24_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "X";
        }

        private void button25_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "Y";
        }

        private void button26_Click(object sender, EventArgs e)
        {
            textBoxSolution.Text += "Z";
        }
    }
}
